/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <immintrin.h> // Required to use intrinsic functions
#include <math.h>
#include <CImg.h>
#include <errno.h>
#include <time.h>
#include <emmintrin.h>

using namespace cimg_library;

// TODO: Example of use of intrinsic functions
// This example doesn't include any code about image processing


#define VECTOR_SIZE   width * height // Array size. 
#define ITEMS_PER_PACKET (sizeof(__m256d)/sizeof(double))
const char* SOURCE_IMG2     ="../Pictures/uniovi_2.bmp";
const char* SOURCE_IMG      ="../Pictures/hojas_2.bmp";
const char* BACKGROUNG_IMG      = "../Pictures/background_bw_l_2.bmp";
const char* DESTINATION_IMG = "../Pictures/HojasRes.bmp";
//__m256d inversoConstante=_mm256_set1_pd(0.7071);
double raiz=sqrt(2.0);
   __m256d inversoConstante= _mm256_set_pd(raiz,raiz,raiz,raiz);

int main() {

    CImg<double> srcImage(SOURCE_IMG);
	CImg<double> srcImage2(BACKGROUNG_IMG);
	
	

	double *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	double *pRsrc2, *pGsrc2, *pBsrc2;
	double *pRdest, *pGdest, *pBdest;
	double *pDstImage; // Pointer to the new image pixels
	uint width, height; // Width and height of the image
	uint width2, height2;
	uint nComp,nComp2; // Number of image components

    struct timespec tStart, tEnd;//COmponenetes para emdir tiempo de algoritmo
	double dElapsedTimes;

	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

// Calculation of the size of the resulting array
    // How many 256 bit packets fit in the array?
    int nPackets = (VECTOR_SIZE * sizeof(double)/sizeof(__m256d));

    // If it is not a exact number we need to add one more packet
    if ( ((VECTOR_SIZE * sizeof(double))%sizeof(__m256d)) != 0) {
        nPackets++;
	}
	// Allocate memory space for destination image components
	pDstImage = (double *) _mm_malloc (sizeof(__m256d) * nPackets, sizeof(__m256d));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}
	printf("Esto es el tamaño d epaquetes %d",nPackets);

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	srcImage2.display(); // Displays the background
	width2  = srcImage2.width(); // Getting information from the source image
	height2 = srcImage2.height();
	nComp2  = srcImage2.spectrum();
	//nComp  = srcImage2.spectrum(); // source image number of components
				// Common values 	
    pRsrc2 = srcImage2.data(); // pRcomp points to the R component array
	pGsrc2 = pRsrc2 + height2 * width2; // pGcomp points to the G component array
	pBsrc2 = pGsrc2 + height2 * width2;
//COmprobamos que las imagenes tenga el mismo tamaño
	if(width!=width2 or height!=height2 or nComp2!=nComp){
		perror("Error de tamaño de imagen");
		exit(-2);
	}

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;
   
    // Create an array aligned to 32 bytes (256 bits) memory boundaries to store the sum.
    // Aligned memory access improves performance    
   // double *resultado = (double *)_mm_malloc(sizeof(__m256d) * nPackets, sizeof(__m256d));

     if(clock_gettime(CLOCK_REALTIME, &tStart) == -1){
		printf("ERROR: clock_gettime: %d.\n",errno);
		exit(EXIT_FAILURE);
	}
	printf("VALOR DE LA INVERSO CONS: %d.\n",inversoConstante);
	//EMPEZAR ALGORITMO
	printf("VALOR DE LA npackets: %d.\n",nPackets);
    for(int i =0; i<nPackets;i++){
		int j=i*ITEMS_PER_PACKET;//256/64 =4*el numero de iteracion
		printf("VALOR DE LA I: %d.\n",i);
		
	 __m256d a = _mm256_loadu_pd(pRsrc +j);
	 __m256d b = _mm256_loadu_pd(pRsrc2+j);
	 __m256d sumaPotencia = _mm256_add_pd(_mm256_mul_pd(a, a), _mm256_mul_pd(b, b));
	 *(__m256d*)(pRdest+j)= _mm256_div_pd(_mm256_sqrt_pd(sumaPotencia),inversoConstante);

	 a = _mm256_loadu_pd(pGsrc+j);
	 b = _mm256_loadu_pd(pGsrc2+j);
	 sumaPotencia = _mm256_add_pd(_mm256_mul_pd(a, a), _mm256_mul_pd(b, b));
	 *(__m256d*)(pGdest+j)=_mm256_div_pd(_mm256_sqrt_pd(sumaPotencia),inversoConstante);

	  a = _mm256_loadu_pd(pBsrc+j);
	 b = _mm256_loadu_pd(pBsrc2+j);
	 sumaPotencia = _mm256_add_pd(_mm256_mul_pd(a, a), _mm256_mul_pd(b, b));
     *(__m256d*)(pBdest+j)=_mm256_div_pd(_mm256_sqrt_pd(sumaPotencia),inversoConstante);

        //pRsrc+=4; pRsrc2+=4;pRdest+=4;
		//pGsrc+=4; pGsrc2+=4;pGdest+=4;
		//pBsrc+=4; pBsrc2+=4;pBdest+=4;
    }
  
    
	if(clock_gettime(CLOCK_REALTIME, &tEnd) == -1){
		printf("ERROR: clock_gettime: %d.\n",errno);
		exit(EXIT_FAILURE);
	}

    dElapsedTimes = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimes += (tEnd.tv_nsec - tStart.tv_nsec)/1e+9;
	//Calculamos  el tiempo del algoritmo,sin incluir inicializacion
	//de las imagenes ni el tiempo de carga
	printf("El tiempo transcurrido es %f",dElapsedTimes);	
	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<double> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save("HojasRes.bmp");
    _mm_free(pDstImage); 

	// Display destination image
	dstImage.display();
	return 0;
}



